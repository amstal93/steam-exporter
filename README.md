# Steam Exporter

Prometheus Exporter for Steam Online Status

## Dependencies

## Environment variables to set

```bash
key         = <Steam API Key>   # 817DFCB0364527FC7E48081506F39C45
steamid     = <Friend Steam ID> # 76561918019934391
steamids    = <SteamID List>    # 76599980193436111,76754546261198046,76198062556428162
```

## Metrics

- steam_persona_state{steamid, personaname, profileurl, avatar, real} 1-6
- steam_last_logoff{steamid, personaname, profileurl, avatar, real} timestamp
- steam_time_created{steamid, personaname, profileurl, avatar, real} timestamp
- steam_playtime_2weeks{steamid, personaname, profileurl, avatar, real, appid, name, img_icon_url, img_logo_url} minutes
- steam_playtime_forever{steamid, personaname, profileurl, avatar, real, appid, name, img_icon_url, img_logo_url} minutes

## Function

### Initial

- Get all Friend Steam IDs where Communityvisibilitystate is public

### Collector

- Check Online State for all SteamIds
- For all SteamIds get Games Owned (Playtime)

