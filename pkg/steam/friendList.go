package steam

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

type steamFriends struct {
	Friendslist struct {
		Friends []struct {
			Steamid      string `json:"steamid"`
			Relationship string `json:"relationship"`
			FriendSince  int    `json:"friend_since"`
		} `json:"friends"`
	} `json:"friendslist"`
}

// Create a Array of SteamIds of all Players that will be monitored
func GetFriendSteamIds(key string, steamid string) []string {
	// Prepare the Steam API Call
	log.Info(fmt.Sprintf("Checking the friends of the steamid: %s", steamid))
	friendURL := fmt.Sprintf(steamFriendApi, key, steamid)
	friendAPIResponse := steamFriends{}

	// Execute the Steam API Call
	err := getJSON(friendURL, &friendAPIResponse)

	// HTTP Error
	if err != nil {
		log.Fatal(err.Error())
	}

	// Add Friends to Steam Id Array
	var steamids []string
	for i := 0; i < len(friendAPIResponse.Friendslist.Friends); i++ {
		steamids = append(steamids, friendAPIResponse.Friendslist.Friends[i].Steamid)
	}

	// Print SteamIds
	log.Info(steamids)

	// Return the array of SteamIds
	return steamids
}
