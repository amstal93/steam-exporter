package steam

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

type steamGameResponse struct {
	Response struct {
		GameCount int         `json:"game_count"`
		Games     []steamGame `json:"games"`
	} `json:"response"`
}

type steamGame struct {
	Appid                    int    `json:"appid"`
	Name                     string `json:"name"`
	PlaytimeForever          int    `json:"playtime_forever"`
	ImgIconURL               string `json:"img_icon_url"`
	ImgLogoURL               string `json:"img_logo_url"`
	PlaytimeWindowsForever   int    `json:"playtime_windows_forever"`
	PlaytimeMacForever       int    `json:"playtime_mac_forever"`
	PlaytimeLinuxForever     int    `json:"playtime_linux_forever"`
	HasCommunityVisibleStats bool   `json:"has_community_visible_stats,omitempty"`
	Playtime2Weeks           int    `json:"playtime_2weeks,omitempty"`
}

func GetPlayerGames(key string, steamid string) []steamGame {
	// Prepare the return array
	log.Info(fmt.Sprintf("Checking the games of the steamid: %s", steamid))

	// Check each Users Game Library
	gameURL := fmt.Sprintf(steamGamesApi, key, steamid)
	log.Info(gameURL)
	gameAPIResponse := steamGameResponse{}

	// Execute the Steam API Call
	err := getJSON(gameURL, &gameAPIResponse)

	// HTTP Error
	if err != nil {
		log.Fatal(err.Error())
	}

	return gameAPIResponse.Response.Games
}
