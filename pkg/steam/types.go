package steam

const steamFriendApi = "http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key=%s&steamid=%s&relationship=friend"
const steamPlayerApi = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=%s&steamids=%s"
const steamGamesApi = "http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001?key=%s&steamid=%s&include_played_free_games=true&include_appinfo=true"
