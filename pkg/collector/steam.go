package collector

import (
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/PatrickDomnick/steam-exporter/pkg/steam"
)

type SteamProfile struct {
	key string
	ids []string

	PersonaState    *prometheus.Desc
	LastLogoff      *prometheus.Desc
	TimeCreated     *prometheus.Desc
	Playtime2Weeks  *prometheus.Desc
	PlaytimeForever *prometheus.Desc
}

func NewSteamProfile(key string, ids []string) *SteamProfile {
	return &SteamProfile{
		key:             key,
		ids:             ids,
		PersonaState:    prometheus.NewDesc(personState, "online state for this user", profileStateLabels, nil),
		LastLogoff:      prometheus.NewDesc(lastLogoff, "timestamp of when the last logoff time for this user was", profileStateLabels, nil),
		TimeCreated:     prometheus.NewDesc(timeCreated, "timestamp of when this user was created", profileStateLabels, nil),
		Playtime2Weeks:  prometheus.NewDesc(playtime2Weeks, "playtime in the last two weeks in minutes for this game for this user", playtimeStateLabels, nil),
		PlaytimeForever: prometheus.NewDesc(playtimeForever, "overall playtime in minutes for this game for this user", playtimeStateLabels, nil),
	}
}

func (s *SteamProfile) Describe(c chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(s, c)
}

func (s *SteamProfile) Collect(c chan<- prometheus.Metric) {
	log.Info("Collecting Metrics for all Players")
	playerProfiles := steam.GetPlayerSummaries(s.key, s.ids)
	for _, pp := range playerProfiles {
		c <- prometheus.MustNewConstMetric(s.PersonaState, prometheus.GaugeValue, float64(pp.Personastate), pp.Steamid, pp.Personaname, pp.Profileurl, pp.Avatar, pp.Realname)
		c <- prometheus.MustNewConstMetric(s.LastLogoff, prometheus.CounterValue, float64(pp.Lastlogoff), pp.Steamid, pp.Personaname, pp.Profileurl, pp.Avatar, pp.Realname)
		c <- prometheus.MustNewConstMetric(s.TimeCreated, prometheus.CounterValue, float64(pp.Timecreated), pp.Steamid, pp.Personaname, pp.Profileurl, pp.Avatar, pp.Realname)

		log.Info("Checking Games for " + pp.Steamid)
		playerGames := steam.GetPlayerGames(s.key, pp.Steamid)
		for _, pg := range playerGames {
			c <- prometheus.MustNewConstMetric(s.Playtime2Weeks, prometheus.GaugeValue, float64(pg.Playtime2Weeks), pp.Steamid, pp.Personaname, pp.Profileurl, pp.Avatar, pp.Realname, strconv.Itoa(pg.Appid), pg.Name, pg.ImgIconURL, pg.ImgLogoURL)
			c <- prometheus.MustNewConstMetric(s.PlaytimeForever, prometheus.CounterValue, float64(pg.PlaytimeForever), pp.Steamid, pp.Personaname, pp.Profileurl, pp.Avatar, pp.Realname, strconv.Itoa(pg.Appid), pg.Name, pg.ImgIconURL, pg.ImgLogoURL)
		}
	}

}
