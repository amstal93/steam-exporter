package collector

const personState = "steam_persona_state"
const lastLogoff = "steam_last_logoff"
const timeCreated = "steam_time_created"
const playtime2Weeks = "steam_playtime_2weeks"
const playtimeForever = "steam_playtime_forever"

var profileStateLabels = []string{"steamid", "personaname", "profileurl", "avatar", "real"}
var playtimeStateLabels = []string{"steamid", "personaname", "profileurl", "avatar", "real", "appid", "name", "img_icon_url", "img_logo_url"}
